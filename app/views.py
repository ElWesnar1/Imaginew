from django.contrib import messages
from django.contrib.auth import authenticate, login, logout, update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate
from django.views.generic import TemplateView
from .forms import ContactForm
from app.forms.register_simple import SignUpForm
from app.models import Gallery


class IndexView(TemplateView):
    template_name = 'index.html'


class profileView(LoginRequiredMixin, TemplateView):
    login_url = 'login'
    template_name = 'profile.html'


class PicturesView(LoginRequiredMixin, TemplateView):
    login_url = 'login'
    template_name = 'Affiche_img.html'
    model = Gallery

    def get_context_data(self, **kwargs):
        result = super().get_context_data(**kwargs)
        result['title'] = 'Titre page'
        result['Example'] = Gallery.objects.all()  # all() = "SELECT * FROM Gallery"

        return result
    # def get_queryset(self):
    #     if 'cat' in self.kwargs:
    #         list = Gallery.objects.filter(categories_title=self.kwargs['cat'])
    #     else:
    #         response = redirect('/')
    #         return response


class PortraitView(LoginRequiredMixin, TemplateView):
    login_url = 'login'
    template_name = 'Affiche_img.html'
    model = Gallery

    def get_context_data(self, **kwargs):
        result = super().get_context_data(**kwargs)
        result['title'] = 'Titre page'
        result['Example'] = Gallery.objects.filter(categories__title='Portrait')
        # all() = "SELECT * FROM Gallery filter on title = Portrait"

        return result


class PaysageView(LoginRequiredMixin, TemplateView):
    login_url = 'login'
    template_name = 'Affiche_img.html'
    model = Gallery

    def get_context_data(self, **kwargs):
        result = super().get_context_data(**kwargs)
        result['title'] = 'Titre page'
        result['Example'] = Gallery.objects.filter(categories__title='Paysage')
        # all() = "SELECT * FROM Gallery filter on title = Paysage"

        return result


class AnimalView(LoginRequiredMixin, TemplateView):
    login_url = 'login'
    template_name = 'Affiche_img.html'
    model = Gallery

    def get_context_data(self, **kwargs):
        result = super().get_context_data(**kwargs)
        result['title'] = 'Titre page'
        result['Example'] = Gallery.objects.filter(categories__title='Animalier')
        # all() = "SELECT * FROM Gallery filter on title = Paysage"

        return result


class FloreView(LoginRequiredMixin, TemplateView):
    login_url = 'login'
    template_name = 'Affiche_img.html'
    model = Gallery

    def get_context_data(self, **kwargs):
        result = super().get_context_data(**kwargs)
        result['title'] = 'Titre page'
        result['Example'] = Gallery.objects.filter(categories__title='Flore')
        # all() = "SELECT * FROM Gallery filter on title = Paysage"

        return result


class LongView(LoginRequiredMixin, TemplateView):
    login_url = 'login'
    template_name = 'Affiche_img.html'
    model = Gallery

    def get_context_data(self, **kwargs):
        result = super().get_context_data(**kwargs)
        result['title'] = 'Titre page'
        result['Example'] = Gallery.objects.filter(categories__title='Pose Longue')
        # all() = "SELECT * FROM Gallery filter on title = Paysage"

        return result

# class PaysageView(LoginRequiredMixin, TemplateView):
#     login_url = 'login'
#     template_name = 'Affiche_img.html'
#     model = Gallery
#
#     def get_context_data(self, **kwargs):
#         result = super().get_context_data(**kwargs)
#         result['title'] = 'Titre page'
#         result['Example'] = Gallery.objects.filter(categories__title='Paysage')
#         # all() = "SELECT * FROM Gallery filter on title = Paysage"
#
#         return result





def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('/')
    else:
        form = SignUpForm()
    return render(request, 'signup.html', {'form': form})


def logout_view(request):
    logout(request)
    response = redirect('/')
    return response


@login_required
def passwordChangeView(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            messages.success(request, 'Your password was successfully updated!')
            return redirect('password_change')
        else:
            messages.error(request, 'Please correct the error below.')
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'passwordChange.html', {
        'form': form
    })

# def contact(request):
#     # Construire le formulaire, soit avec les données postées,
#     # soit vide si l'utilisateur accède pour la première fois
#     # à la page.
#     form = ContactForm(request.POST or None)
#     # Nous vérifions que les données envoyées sont valides
#     # Cette méthode renvoie False s'il n'y a pas de données
#     # dans le formulaire ou qu'il contient des erreurs.
#     if form.is_valid():
#         # Ici nous pouvons traiter les données du formulaire
#         sujet = form.cleaned_data['sujet']
#         message = form.cleaned_data['message']
#         envoyeur = form.cleaned_data['envoyeur']
#         renvoi = form.cleaned_data['renvoi']
#
#         # Nous pourrions ici envoyer l'e-mail grâce aux données
#         # que nous venons de récupérer
#         envoi = True
#
#     # Quoiqu'il arrive, on affiche la page du formulaire.
#     return render(request, 'blog/contact.html', locals())
