from django.contrib.auth.models import User
from django.db import models
from django.db.models import ImageField


# Create your models here


class Category(models.Model):
    title = models.CharField(max_length=50, blank=False, null=False, default=None)
    description = models.CharField(max_length=200, blank=True, null=True, default=None)

    def __str__(self):
        return f'{str(self.title)}'


class Gallery(models.Model):
    title = models.CharField(max_length=50)
    description = models.TextField(max_length=300)
    image = ImageField(upload_to="gallery", height_field=None, width_field=None)
    categories = models.ForeignKey(Category, on_delete=models.CASCADE, blank=False, null=False, default=None)

    def __str__(self):
        result = self.title
        return result
