from django.contrib import admin

# Register your models here.
from app.models import Gallery, Category

admin.site.register(Gallery)
admin.site.register(Category)
