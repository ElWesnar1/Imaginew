# Generated by Django 3.0.2 on 2020-01-30 17:18

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0005_remove_category_title'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='title',
            field=models.CharField(default=None, max_length=50),
        ),
        migrations.AddField(
            model_name='gallery',
            name='categories',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='app.Category'),
        ),
    ]
