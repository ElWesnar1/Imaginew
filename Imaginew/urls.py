"""Imaginew URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include
from django.contrib.auth import views as auth_views
from . import settings
from django.contrib.staticfiles.urls import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf.urls.i18n import i18n_patterns

from app.views import IndexView, signup, logout_view, PicturesView, PortraitView, PaysageView, passwordChangeView, \
    profileView, AnimalView, FloreView, LongView

urlpatterns = [
    path('i18n/', include('django.conf.urls.i18n')),
    path('admin/', admin.site.urls),
]
urlpatterns += i18n_patterns(
    url(r'^$', IndexView.as_view(), name='app_index'),
    # url(r'^not_connected$', IndexViewDisconnect.as_view(), name='app_index'),
    url(r'^signup/$', signup, name='signup'),
    url(r'^login/$', auth_views.LoginView.as_view(template_name='login.html'), name='login'),
    url(r'^logout/$', logout_view, name='logout'),
    # url(r'^pictures/<str:cat>', PicturesView, name='portrait'),
    url(r'^pictures/$', PicturesView.as_view(), name='pictures'),
    url(r'^pictures/portrait$', PortraitView.as_view(), name='portrait'),
    url(r'^pictures/paysage$', PaysageView.as_view(), name='paysage'),
    url(r'^pictures/animalier$', AnimalView.as_view(), name='animalier'),
    url(r'^pictures/flore$', FloreView.as_view(), name='flore'),
    url(r'^pictures/pose_longue$', LongView.as_view(), name='pose_longue'),
    url(r'^profile/password/$', passwordChangeView, name='password_change'),
    url(r'^profile/$', profileView.as_view(), name='profile'),
)

urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
